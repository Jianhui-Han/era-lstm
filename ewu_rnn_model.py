import torch
import torch.nn as nn


class EwuRNN(nn.Module):
    def __init__(self, nhid, rmax):
        super(EwuRNN, self).__init__()
        self.layer1 = nn.Linear(5, nhid)
        self.layer2 = nn.Linear(nhid, 2)
        self.rmax = rmax

    def forward(self, inputs):
        """ inputs should have size (batch_size, num_iter, 4) """
        cx = torch.zeros(inputs.size(0), 1, 1).cuda()
        outputs = torch.zeros(inputs.size(0), inputs.size(1), 2).cuda()
        for i in range(inputs.size(1)):
            t0 = torch.cat((inputs[:, i:i+1, :], cx), 2)
            t1 = self.layer1(t0)
            t2 = torch.sigmoid(t1)
            t3 = self.layer2(t2)
            t4 = torch.clamp(t3, -self.rmax, self.rmax)
            cx = t4[:, :, 1:2]
            outputs[:, i:i+1, :] = t4
        return outputs
