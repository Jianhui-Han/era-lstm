import time
import torch

import ewu_mlp_model

import argparse

nhid = 256
batch_size = 4096
num_train = 7000
num_eval = 3000
num_test = 5000
num_epoch = 50

learning_rate = 10e-5

torch.manual_seed(1701)

add_noise = True
quantize_bits = 5

parser = argparse.ArgumentParser(description='training mlp approximator')
parser.add_argument('--nhid', type=int, default=128)
parser.add_argument('--qb', type=int, default=8)
args = parser.parse_args()

print("noise added, hidden size %d, quantize with %d bits" % (args.nhid, args.qb))
_model_save = 'model/ewu_new/ewu_mlp_h' + str(args.nhid) + '_noise_q' + str(args.qb) + '_'
print('saving to model: %s' % _model_save)

def criterion(x1, x2):
    return torch.mean((x1-x2) ** 2)


def relative_error(x1, x2):
    err = x1 - x2
    err_total = torch.mean(torch.abs(err))
    base_total = torch.mean(torch.abs(x2))
    return err_total / base_total


inputs = torch.rand((num_train+num_eval+num_test, batch_size, 5)).cuda()
inputs[:, :, 2] = inputs[:, :, 2] * 2 - 1
inputs[:, :, 4] = inputs[:, :, 4] * 2 - 1
targets = torch.zeros((num_train+num_eval+num_test, batch_size, 2)).cuda()
targets[:, :, 1] = inputs[:, :, 0] * inputs[:, :, 2] + inputs[:, :, 4] * inputs[:, :, 1]
targets[:, :, 0] = torch.tanh(targets[:, :, 1]) * inputs[:, :, 3]

model = ewu_mlp_model.EwuMLP(args.nhid).cuda()
model.add_noise = add_noise
# model.quantize_bits = quantize_bits
model.quantize_bits = args.qb
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

best_val_loss = None

for epoch in range(num_epoch):
    epoch_start_time = time.perf_counter()
    start_time = time.perf_counter()
    total_loss = 0.
    model.train()

    for batch in range(num_train):
        optimizer.zero_grad()
        outputs = model(inputs[batch, :, :])
        loss = criterion(targets[batch, :], outputs)
        total_loss += loss.item()
        loss.backward()
        optimizer.step()

        if batch % 1000 == 999:
            print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:1.8f} '
                  .format(epoch, batch+1, num_train, learning_rate, (time.perf_counter()-start_time),
                          total_loss/(batch+1)))
            start_time = time.perf_counter()

    total_loss = 0.
    total_re = 0.
    model.eval()
    for batch in range(num_train, num_train+num_eval):
        with torch.no_grad():
            outputs = model(inputs[batch, :, :])
            loss = criterion(targets[batch, :, :], outputs)
            total_loss += loss.item()
            total_re += relative_error(outputs, targets[batch, :, :])

    print('-' * 80)
    print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:1.8f} | relative error {:1.8f}'
          .format(epoch, (time.perf_counter() - epoch_start_time), total_loss / num_eval, total_re / num_eval))
    print('-' * 80)

    if not best_val_loss or total_loss < best_val_loss:
        with open(_model_save, 'wb') as f:
            torch.save(model, f)
        print('save model: %s' % _model_save)
        learning_rate *= 1.01
        best_val_loss = total_loss
    else:
        learning_rate /= 2
    for param_group in optimizer.param_groups:
        param_group['lr'] = learning_rate

    # if epoch > 20:
    #     model.quantize = quantize


with open(_model_save, 'rb') as f:
    winner = torch.load(f)
total_loss = 0
total_re = 0
for batch in range(num_train+num_eval, num_train+num_eval+num_test):
    with torch.no_grad():
        outputs = winner(inputs[batch, :, :])
        loss = criterion(targets[batch, :, :], outputs)
        total_loss += loss
        total_re += relative_error(outputs, targets[batch, :, :])

print('=' * 80)
print('| End of training | test loss {:1.8f} | test relative error {:1.8f}'.format(
    total_loss/num_test, total_re/num_test))
print(_model_save)
print('=' * 80)
