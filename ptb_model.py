import torch
import torch.nn as nn


class PTB(nn.Module):
    def __init__(self, ntoken, ninp, nhid, ewu_model, rmax, dropout=0.5):
        super(PTB, self).__init__()
        self.nhid = nhid

        self.dropout = nn.Dropout(dropout)
        self.encoder = nn.Embedding(ntoken, ninp)
        self.linear_ih_1 = nn.Linear(ninp, 4*nhid)
        self.linear_hh_1 = nn.Linear(ninp, 4*nhid)
        self.linear_ih_2 = nn.Linear(nhid, 4*nhid)
        self.linear_hh_2 = nn.Linear(nhid, 4*nhid)
        self.decoder = nn.Linear(nhid, ntoken)

        with open(ewu_model, 'rb') as f:
            self.ewu = torch.load(f)
        self.ewu_layer1 = self.ewu.layer1
        self.ewu_layer2 = self.ewu.layer2
        self.ewu_saved = [self.ewu.layer1.weight.data.clone(), self.ewu.layer1.bias.data.clone(),
                          self.ewu.layer2.weight.data.clone(), self.ewu.layer2.bias.data.clone()]
        self.rmax = rmax

        self.is_ft_ewu = False

        self.add_noise = False
        self.weight_saved = []

        self.io_bits = 0
        self.quantize_bits = 0

        self.variation = 0

        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, inputs, hidden):
        if self.add_noise:
            if self.variation > 0:
                self.weight_add_variation()
            else:
                self.weight_quantize_noise()

        emb = self.dropout(self.encoder(inputs))

        if self.add_noise > 0:
            emb = self.io_constraint(emb)

        outputs, hid1 = self.lstm_forward(emb, hidden[0], self.linear_ih_1, self.linear_hh_1)
        outputs = self.dropout(outputs)
        outputs, hid2 = self.lstm_forward(outputs, hidden[1], self.linear_ih_2, self.linear_hh_2)
        outputs = self.dropout(outputs)
        decoded = self.decoder(outputs)

        if self.add_noise:
            decoded = self.io_constraint(decoded)
            self.weight_restore()

        return decoded, (hid1, hid2)

    def lstm_forward(self, inputs, hidden, linear_ih, linear_hh):
        if not self.is_ft_ewu:
            self.ewu_restore()

        outputs = []
        res = hidden

        if self.add_noise:
            inputs = self.io_constraint(inputs)

        for k in range(inputs.size(0)):
            hx, cx = res
            gates = linear_ih(inputs[k]) + linear_hh(hx)
            ingate, forgetgate, cellgate, outgate = gates.chunk(4, 2)
            it = torch.sigmoid(ingate)
            ft = torch.sigmoid(forgetgate)
            gt = torch.tanh(cellgate)
            ot = torch.sigmoid(outgate)
            temp_in = torch.cat((it.view(inputs.size(1), inputs.size(2), -1),
                                 ft.view(inputs.size(1), inputs.size(2), -1),
                                 gt.view(inputs.size(1), inputs.size(2), -1),
                                 ot.view(inputs.size(1), inputs.size(2), -1),
                                 cx.view(inputs.size(1), inputs.size(2), -1)), 2)

            temp_out1 = self.ewu(temp_in)
            temp_out2 = torch.clamp(temp_out1, -self.rmax, self.rmax)
            hy = temp_out2[:, :, 0].view(-1, inputs.size(1), inputs.size(2))
            cy = temp_out2[:, :, 1].view(-1, inputs.size(1), inputs.size(2))

            if self.add_noise > 0:
                hy = self.io_constraint(hy)
                cy = self.io_constraint(cy)

            outputs.append(hy)
            res = (hy, cy)

        outputs = torch.cat(outputs, 0)
        return outputs, res

    def ewu_restore(self):
        self.ewu_layer1.weight.data = self.ewu_saved[0].clone()
        self.ewu_layer1.bias.data = self.ewu_saved[1].clone()
        self.ewu_layer2.weight.data = self.ewu_saved[2].clone()
        self.ewu_layer2.bias.data = self.ewu_saved[3].clone()

    def init_hidden(self, bsz):
        weight = next(self.parameters())

        return [(weight.new_zeros(1, bsz, self.nhid), weight.new_zeros(1, bsz, self.nhid)),
                (weight.new_zeros(1, bsz, self.nhid), weight.new_zeros(1, bsz, self.nhid))]

    def weight_add_noise(self):
        for submodule in (self.linear_ih_1, self.linear_hh_1, self.linear_ih_2, self.linear_hh_2,
                          self.encoder, self.decoder):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span)
            std = (-0.0006034 * (40 * submodule.weight.data + 4) ** 2 +
                   0.06184 * (40 * submodule.weight.data + 4) + 0.7240) / 40
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += std * rand
            submodule.weight.data *= float(half_span)

    def weight_add_variation(self):
        maximum = (2 ** self.quantize_bits - 1) / 2
        for submodule in (self.linear_ih_1, self.linear_hh_1, self.linear_ih_2, self.linear_hh_2,
                          self.encoder, self.decoder):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span) * maximum
            submodule.weight.data = torch.round(submodule.weight.data) / maximum
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += self.variation * rand
            submodule.weight.data *= float(half_span)

    def weight_restore(self):
        cnt = 0
        for submodule in (self.linear_ih_1, self.linear_hh_1, self.linear_ih_2, self.linear_hh_2,
                          self.encoder, self.decoder):
            submodule.weight.data = self.weight_saved[cnt].clone()
            cnt += 1
        self.weight_saved = []

    def weight_quantize_noise(self):
        maximum = (2 ** self.quantize_bits - 1) / 2
        for submodule in (self.linear_ih_1, self.linear_hh_1, self.linear_ih_2, self.linear_hh_2,
                          self.encoder, self.decoder):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            half_span = 8 if half_span > 8 else half_span
            submodule.weight.data = submodule.weight.data / float(half_span) * maximum
            submodule.weight.data = torch.round(submodule.weight.data) / maximum
            std = (-0.0006034 * (40 * submodule.weight.data + 4) ** 2 +
                   0.06184 * (40 * submodule.weight.data + 4) + 0.7240) / 40
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += std * rand
            submodule.weight.data *= float(half_span)

    def io_constraint(self, x):
        maximum = (2 ** self.io_bits - 1) / 2
        half_span = torch.max(torch.abs(x))
        x.data = x.data / half_span * maximum
        x.data = torch.round(x.data)
        x.data = x.data / maximum * half_span
        return x
