import torch
import torch.nn as nn

import analog


class EwuMLP(nn.Module):
    def __init__(self, nhid):
        super(EwuMLP, self).__init__()
        self.layer1 = nn.Linear(5, nhid)
        self.layer2 = nn.Linear(nhid, 2)
        self.add_noise = False
        self.quantize_bits = 0
        self.weight_saved = []

    def forward(self, inputs):
        if self.add_noise:
            if self.quantize_bits == 0:
                print("Error: quantize_bits not set")
                exit(0)
            self.quantize_weight()

        outputs1 = self.layer1(inputs)
        if self.add_noise:
            outputs2 = analog.MySigmoid.apply(outputs1)
        else:
            outputs2 = torch.sigmoid(outputs1)
        outputs3 = self.layer2(outputs2)

        if self.add_noise:
            self.restore_weight()

        return outputs3

    def quantize_weight(self):
        maximum = (2 ** self.quantize_bits - 1) / 2
        for submodule in (self.layer1, self.layer2):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span) * maximum
            submodule.weight.data = torch.round(submodule.weight.data)
            submodule.weight.data = submodule.weight.data / maximum * half_span

    def restore_weight(self):
        self.layer1.weight.data = self.weight_saved[0].clone()
        self.layer2.weight.data = self.weight_saved[1].clone()
        self.weight_saved = []
