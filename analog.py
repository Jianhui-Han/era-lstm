import torch


class MySigmoid(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        def left(x):
            return -5.557e-06 * x**5 - 0.0002819 * x**4 - 0.005495 * x**3 - 0.05106 * x**2 - 0.2196 * x - 0.3003

        def middle(x):
            return -0.3361 * x**3 + 0.06143 * x**2 + 0.3994 * x - 0.02985

        def right(x):
            return -5.459e-06 * x**5 + 0.0002776 * x**4 - 0.005427 * x**3 + 0.05061 * x**2 - 0.2167 * x + 0.275

        mask_left = ((x >= -16) & (x < -0.48)).float()
        mask_middle = ((x >= -0.48) & (x <= 0.48)).float()
        mask_right = ((x > 0.48) & (x <= 16)).float()
        error = left(x) * mask_left + middle(x) * mask_middle + right(x) * mask_right
        y = torch.sigmoid(x)
        ctx.save_for_backward(x, y)
        return error + y

    @staticmethod
    def backward(ctx, grad_output):
        def left(x):
            return -2.355e-05 * x**4 - 0.0009949 * x**3 - 0.0151 * x**2 - 0.09654 * x - 0.2124

        def middle(x):
            return -0.2591 * x**3 - 1.027 * x**2 - 0.03038 * x + 0.3891

        def right(x):
            return -2.432e-05 * x**4 + 0.001025 * x**3 - 0.01555 * x**2 + 0.09959 * x - 0.2183

        x, y = ctx.saved_tensors
        mask_left = ((x >= -16) & (x < -0.80)).float()
        mask_middle = ((x >= -0.80) & (x <= 0.64)).float()
        mask_right = ((x > 0.64) & (x <= 16)).float()
        error = left(x) * mask_left + middle(x) * mask_middle + right(x) * mask_right
        return grad_output * (error + y * (1 - y))


class MyHypertan(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        def left(x):
            return 0.01168 * x + 0.1126

        def middle(x):
            return 0.1927 * x**3 + 0.0001727 * x**2 - 0.3377 * x - 3.762e-05

        def right(x):
            return 0.01156 * x - 0.1113

        mask_left = ((x >= -8) & (x < -1.12)).float()
        mask_middle = ((x >= -1.12) & (x <= 1.12)).float()
        mask_right = ((x > 1.12) & (x <= 8)).float()
        error = left(x) * mask_left + middle(x) * mask_middle + right(x) * mask_right
        y = torch.tanh(x)
        ctx.save_for_backward(x, y)
        return error + y

    @staticmethod
    def backward(ctx, grad_output):
        def left(x):
            return 0.001607 * x**3 + 0.02471 * x**2 + 0.1195 * x + 0.1909

        def middle(x):
            return -0.08464 * x**5 - 0.4076 * x**4 + 0.06936 * x**3 + 0.9469 * x**2 - 0.01132 * x - 0.3736

        def right(x):
            return 0.0007899 * x**3  - 0.01317 * x**2 + 0.06818 *x - 0.0962

        x, y = ctx.saved_tensors
        mask_left = ((x >= -8) & (x < -1.12)).float()
        mask_middle = ((x >= -1.12) & (x <= 1.12)).float()
        mask_right = ((x > 1.12) & (x <= 8)).float()
        error = left(x) * mask_left + middle(x) * mask_middle + right(x) * mask_right
        return grad_output * (error + (1 - y * y))
