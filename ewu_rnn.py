import time
import torch

import ewu_rnn_model


nhid = 128
rmax = 8

num_iter = 100
batch_size = 100
num_train = 700
num_eval = 300
num_test = 500
num_epoch = 100

learning_rate = 0.1

torch.manual_seed(1701)


def criterion(x1, x2):
    return torch.mean((x1-x2) ** 2)


def relative_error(x1, x2):
    err = x1 - x2
    err_total = torch.mean(torch.abs(err))
    base_total = torch.mean(torch.abs(x2))
    return err_total / base_total


def generate_data():
    inputs = torch.rand(1, batch_size, num_iter, 4).cuda()
    inputs[:, :, :, 2] = inputs[:, :, :, 2] * 2 - 1
    cx = torch.zeros(1, batch_size, 1, 1).cuda()
    targets = torch.zeros((1, batch_size, num_iter, 2)).cuda()
    for i in range(num_iter):
        t0 = inputs[:, :, i:i+1, 1:2] * cx + inputs[:, :, i:i+1, 0:1] * inputs[:, :, i:i+1, 2:3]
        t1 = torch.tanh(t0) * inputs[:, :, i:i+1, 3:4]
        targets[:, :, i:i+1, 0:1] = torch.clamp(t1, -rmax, rmax)
        targets[:, :, i:i+1, 1:2] = torch.clamp(t0, -rmax, rmax)
        cx = targets[:, :, i:i+1, 1:2]
    return inputs, targets


model = ewu_rnn_model.EwuRNN(nhid, rmax).cuda()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
best_val_loss = None

for epoch in range(num_epoch):
    epoch_start_time = time.perf_counter()

    start_time = time.perf_counter()
    total_loss = 0.
    model.train()
    for batch in range(num_train):
        inputs, targets = generate_data()
        optimizer.zero_grad()
        outputs = model(inputs[0, :, :, :])
        loss = criterion(targets[0, :, :, :], outputs)
        total_loss += loss.item()
        loss.backward()
        optimizer.step()

        if batch % 100 == 99:
            print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:1.8f} '
                  .format(epoch, batch+1, num_train, learning_rate, (time.perf_counter()-start_time)/100,
                          total_loss/(batch+1)))
            start_time = time.perf_counter()

    total_loss = 0.
    model.eval()
    for batch in range(num_train, num_train+num_eval):
        inputs, targets = generate_data()
        with torch.no_grad():
            outputs = model(inputs[0, :, :, :])
            loss = criterion(targets[0, :, :, :], outputs)
            total_loss += loss.item()

    print('-' * 80)
    print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:1.8f} '.format(
        epoch, (time.perf_counter() - epoch_start_time), total_loss/num_eval))
    print('-' * 80)

    if not best_val_loss or total_loss < best_val_loss:
        with open('model/ewu_rnn.save', 'wb') as f:
            torch.save(model, f)
        print('save model: model/ewu_rnn.save')
        learning_rate *= 1.01
        best_val_loss = total_loss
    else:
        learning_rate /= 2
    for param_group in optimizer.param_groups:
        param_group['lr'] = learning_rate


with open('model/ewu_rnn.save', 'rb') as f:
    winner = torch.load(f)
total_loss = 0
total_re = 0
for batch in range(num_train+num_eval, num_train+num_eval+num_test):
    with torch.no_grad():
        inputs, targets = generate_data()
        outputs = winner(inputs[0, :, :, :])
        loss = criterion(targets[0, :, :, :], outputs)
        total_loss += loss
        total_re += relative_error(outputs, targets[0, :, :, :])

print('=' * 80)
print('| End of training | test loss {:1.8f} | test relative error {:1.8f}'.format(
    total_loss/num_test, total_re/num_test))
print('=' * 80)
