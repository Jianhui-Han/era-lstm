import math
import time
import torch
import torch.nn as nn

import ptb_data
import ptb_model


log_interval = 200
model_save = 'model/ptb_ewumlp_noise_ft_io8_q8_8142'

add_noise = True
io_bits = 8
quantize_bits = 8
variation = 0.05

ewu_model = 'model/ewu_mlp_re41'
rmax = 8

lr = 20         # learning rate
bsz = 20        # batch size
nepoch = 10     # number of epochs
emsize = 1500   # size of word embeddings
nhid = 1500     # size of hidden units per layer
dropout = 0.65  # dropout applied to layers
bptt = 35       # sequence length
clip = 0.25     # gradient clipping

is_ft = True
base_model = 'model/ptb_bsl_7819'
is_ft_ewu = False

torch.manual_seed(1111)


# Starting from sequential data, batchify arranges the dataset into columns.
# For instance, with the alphabet as the sequence and batch size 4, we'd get
# ┌ a g m s ┐
# │ b h n t │
# │ c i o u │
# │ d j p v │
# │ e k q w │
# └ f l r x ┘.
# These columns are treated as independent by the model, which means that the
# dependence of e. g. 'g' on 'f' can not be learned, but allows more efficient
# batch processing.


def batchify(data, bsz):
    # Work out how cleanly we can divide the dataset into bsz parts.
    nbatch = data.size(0) // bsz
    # Trim off any extra elements that wouldn't cleanly fit (remainders).
    data = data.narrow(0, 0, nbatch * bsz)
    # Evenly divide the data across the bsz batches.
    data = data.view(bsz, -1).t().contiguous()
    return data.cuda()


def repackage_hidden(h):
    """Wraps hidden states in new Variables, to detach them from their history."""
    if isinstance(h, torch.Tensor):
        return h.detach()
    else:
        return tuple(repackage_hidden(v) for v in h)


# get_batch subdivides the source data into chunks of length args.bptt.
# If source is equal to the example output of the batchify function, with
# a bptt-limit of 2, we'd get the following two Variables for i = 0:
# ┌ a g m s ┐ ┌ b h n t ┐
# └ b h n t ┘ └ c i o u ┘
# Note that despite the name of the function, the subdivison of data is not
# done along the batch dimension (i.e. dimension 1), since that was handled
# by the batchify function. The chunks are along dimension 0, corresponding
# to the seq_len dimension in the LSTM.

def get_batch(source, i):
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].view(-1)
    return data, target


def train():
    # Turn on training mode which enables dropout.
    model.train()
    total_loss = 0.
    start_time = time.time()
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(bsz)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, bptt)):
        data, targets = get_batch(train_data, i)
        # Starting each batch, we detach the hidden state from how it was previously produced.
        # If we didn't, the model would try backpropagating all the way to start of the dataset.
        hidden = repackage_hidden(hidden)
        model.zero_grad()
        output, hidden = model(data, hidden)
        loss = criterion(output.view(-1, ntokens), targets)
        loss.backward()

        # `clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
        torch.nn.utils.clip_grad_norm_(model.parameters(), clip)
        for p in model.parameters():
            if p.grad is not None:
                p.data.add_(-lr, p.grad.data)

        total_loss += loss.item()

        if batch % log_interval == 0 and batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | '
                  'loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // bptt, lr,
                                                      elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


def evaluate(data_source):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    total_loss = 0.
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(eval_batch_size)
    with torch.no_grad():
        for i in range(0, data_source.size(0) - 1, bptt):
            data, targets = get_batch(data_source, i)
            output, hidden = model(data, hidden)
            output_flat = output.view(-1, ntokens)
            total_loss += len(data) * criterion(output_flat, targets).item()
            hidden = repackage_hidden(hidden)
    return total_loss / len(data_source)


corpus = ptb_data.Corpus('./ptb_data/penn')
ntokens = len(corpus.dictionary)

eval_batch_size = 10
train_data = batchify(corpus.train, bsz)
val_data = batchify(corpus.valid, eval_batch_size)
test_data = batchify(corpus.test, eval_batch_size)

model = ptb_model.PTB(ntokens, emsize, nhid, ewu_model, rmax, dropout=dropout).cuda()
criterion = nn.CrossEntropyLoss()

if is_ft:
    print('Fine-tuning: use model ' + base_model)
    with open(base_model, 'rb') as f:
        model_old = torch.load(f)
    model.linear_ih_1.weight = model_old.linear_ih_1.weight
    model.linear_ih_1.bias = model_old.linear_ih_1.bias
    model.linear_hh_1.weight = model_old.linear_hh_1.weight
    model.linear_hh_1.bias = model_old.linear_hh_1.bias
    model.linear_ih_2.weight = model_old.linear_ih_2.weight
    model.linear_ih_2.bias = model_old.linear_ih_2.bias
    model.linear_hh_2.weight = model_old.linear_hh_2.weight
    model.linear_hh_2.bias = model_old.linear_hh_2.bias
    model.encoder.weight = model_old.encoder.weight
    model.decoder.weight = model_old.decoder.weight
    model.decoder.bias = model_old.decoder.bias
    model.is_ft_ewu = is_ft_ewu
model.add_noise = add_noise
model.io_bits = io_bits
model.quantize_bits = quantize_bits
model.ewu.add_noise = add_noise
model.ewu.quantize_bits = quantize_bits


# best_val_loss = None
# for epoch in range(1, nepoch + 1):
#     epoch_start_time = time.time()
#     train()
#     val_loss = evaluate(val_data)
#     print('-' * 89)
#     print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
#           'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
#                                      val_loss, math.exp(val_loss)))
#     print('-' * 89)
#     # Save the model if the validation loss is the best we've seen so far.
#     if not best_val_loss or val_loss < best_val_loss:
#         with open(model_save, 'wb') as f:
#             torch.save(model, f)
#         print('save model: ' + model_save)
#         best_val_loss = val_loss
#     else:
#         # Anneal the learning rate if no improvement has been seen in the validation dataset.
#         lr /= 4.0

with open(model_save, 'rb') as f:
    model = torch.load(f)
    print('loaded model: ', model_save)
    model.add_noise = add_noise
    model.io_bits = io_bits
    model.quantize_bits = quantize_bits
    model.variation = variation
    model.ewu.add_noise = add_noise
    model.ewu.quantize_bits = quantize_bits

# Run on test data.
test_loss = evaluate(test_data)
print('=' * 89)
print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(
    test_loss, math.exp(test_loss)))
print('=' * 89)



