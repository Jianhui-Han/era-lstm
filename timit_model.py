import torch
import torch.nn as nn

import analog


class TIMIT(nn.Module):
    def __init__(self, ninp, nhid, nout, ewu_model, rmax):
        super(TIMIT, self).__init__()
        self.nhid = nhid
        self.ndir = 2

        self.linear_ih = nn.Linear(ninp, 4*nhid)
        self.linear_hh = nn.Linear(nhid, 4*nhid)
        self.linear_ih_re = nn.Linear(ninp, 4*nhid)
        self.linear_hh_re = nn.Linear(nhid, 4*nhid)

        self.linear_fc = nn.Linear(2*nhid, nout)

        with open(ewu_model, 'rb') as f:
            self.ewu = torch.load(f)
        self.ewu_layer1 = self.ewu.layer1
        self.ewu_layer2 = self.ewu.layer2
        self.ewu_saved = [self.ewu.layer1.weight.data.clone(), self.ewu.layer1.bias.data.clone(),
                          self.ewu.layer2.weight.data.clone(), self.ewu.layer2.bias.data.clone()]
        self.rmax = rmax

        self.is_ft_ewu = False

        self.add_noise = False
        self.weight_saved = []

        self.io_bits = 0
        self.quantize_bits = 0

        self.variation = 0
        self.fluctuation = 0

        initrange = 0.1
        self.linear_ih.weight.data.uniform_(-initrange, initrange)
        self.linear_ih.bias.data.uniform_(-initrange, initrange)
        self.linear_hh.weight.data.uniform_(-initrange, initrange)
        self.linear_hh.bias.data.uniform_(-initrange, initrange)
        self.linear_ih_re.weight.data.uniform_(-initrange, initrange)
        self.linear_ih_re.bias.data.uniform_(-initrange, initrange)
        self.linear_hh_re.weight.data.uniform_(-initrange, initrange)
        self.linear_hh_re.bias.data.uniform_(-initrange, initrange)
        self.linear_fc.weight.data.uniform_(-initrange, initrange)
        self.linear_fc.bias.data.uniform_(-initrange, initrange)

    def forward(self, inputs):
        if not self.is_ft_ewu:
            self.ewu_restore()

        if self.add_noise:
            if self.variation > 0:
                self.weight_add_variation()
            else:
                self.weight_quantize_noise()

        h0 = torch.zeros(self.ndir, 1, inputs.size(0), self.nhid).cuda()
        c0 = torch.zeros(self.ndir, 1, inputs.size(0), self.nhid).cuda()
        hidden = list(zip(h0, c0))

        x = inputs.transpose(0, 1)
        x = self.io_constraint(x)

        outputs_lstm = []

        for d in range(self.ndir):
            steps = range(x.size(0)-1, -1, -1) if d > 0 else range(x.size(0))
            res = hidden[d]
            outputs = []
            for k in steps:
                hx, cx = res
                if d > 0:
                    gates = self.linear_ih_re(x[k]) + self.linear_hh_re(hx)
                else:
                    gates = self.linear_ih(x[k]) + self.linear_hh(hx)
                ingate, forgetgate, cellgate, outgate = gates.chunk(4, 2)
                if self.add_noise:
                    it = analog.MySigmoid.apply(ingate)
                    ft = analog.MySigmoid.apply(forgetgate)
                    gt = analog.MyHypertan.apply(cellgate)
                    ot = analog.MySigmoid.apply(outgate)
                else:
                    it = torch.sigmoid(ingate)
                    ft = torch.sigmoid(forgetgate)
                    gt = torch.tanh(cellgate)
                    ot = torch.sigmoid(outgate)
                temp_in = torch.cat((it.view(inputs.size(0), self.nhid, -1),
                                     ft.view(inputs.size(0), self.nhid, -1),
                                     gt.view(inputs.size(0), self.nhid, -1),
                                     ot.view(inputs.size(0), self.nhid, -1),
                                     cx.view(inputs.size(0), self.nhid, -1)), 2)

                temp_out1 = self.ewu(temp_in)
                temp_out2 = torch.clamp(temp_out1, -self.rmax, self.rmax)
                hy = temp_out2[:, :, 0].view(-1, inputs.size(0), self.nhid)
                cy = temp_out2[:, :, 1].view(-1, inputs.size(0), self.nhid)

                # cy = torch.clamp((it * gt + ft * cx).view(-1, inputs.size(0), self.nhid), self.rmax)
                # hy = torch.clamp((torch.tanh(cy) * ot).view(-1, inputs.size(0), self.nhid), self.rmax)

                if self.add_noise:
                    hy = self.io_constraint(hy)
                    cy = self.io_constraint(cy)

                outputs.append(hy)
                res = (hy, cy)

            if d > 0:
                outputs.reverse()
            outputs = torch.cat(outputs, 0)
            outputs_lstm.append(outputs)

        outputs_lstm = torch.cat(outputs_lstm, 2).transpose(0, 1)
        outputs_fc = self.linear_fc(outputs_lstm)

        if self.add_noise:
            outputs_fc = self.io_constraint(outputs_fc)
            self.weight_restore()

        return outputs_fc

    def ewu_restore(self):
        self.ewu_layer1.weight.data = self.ewu_saved[0].clone()
        self.ewu_layer1.bias.data = self.ewu_saved[1].clone()
        self.ewu_layer2.weight.data = self.ewu_saved[2].clone()
        self.ewu_layer2.bias.data = self.ewu_saved[3].clone()

    def weight_add_noise(self):
        for submodule in (self.linear_ih, self.linear_hh, self.linear_ih_re, self.linear_hh_re, self.linear_fc):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span)
            std = (-0.0006034 * (40 * submodule.weight.data + 4) ** 2 +
                   0.06184 * (40 * submodule.weight.data + 4) + 0.7240) / 40
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += std * rand
            submodule.weight.data *= float(half_span)

    def weight_quantize_noise(self):
        maximum = (2 ** self.quantize_bits - 1) / 2
        for submodule in (self.linear_ih, self.linear_hh, self.linear_ih_re, self.linear_hh_re, self.linear_fc):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span) * maximum
            submodule.weight.data = torch.round(submodule.weight.data) / maximum
            std = (-0.0006034 * (40 * submodule.weight.data + 4) ** 2 +
                   0.06184 * (40 * submodule.weight.data + 4) + 0.7240) / 40
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += std * rand
            submodule.weight.data *= float(half_span)

    def weight_add_variation(self):
        maximum = (2 ** self.quantize_bits - 1) / 2
        for submodule in (self.linear_ih, self.linear_hh, self.linear_ih_re, self.linear_hh_re, self.linear_fc):
            self.weight_saved.append(submodule.weight.data.clone())
            half_span = torch.max(torch.abs(submodule.weight.data))
            submodule.weight.data = submodule.weight.data / float(half_span) * maximum
            submodule.weight.data = torch.round(submodule.weight.data) / maximum
            rand = torch.Tensor(submodule.weight.data.shape).normal_().cuda()
            submodule.weight.data += self.variation * rand / 2.82
            submodule.weight.data *= float(half_span)

    def weight_restore(self):
        cnt = 0
        for submodule in (self.linear_ih, self.linear_hh, self.linear_ih_re, self.linear_hh_re, self.linear_fc):
            submodule.weight.data = self.weight_saved[cnt].clone()
            cnt += 1
        self.weight_saved = []

    def io_constraint(self, x):
        maximum = (2 ** self.io_bits - 1) / 2
        half_span = torch.max(torch.abs(x))
        x.data = x.data / half_span * maximum
        x.data = torch.round(x.data)
        rand = torch.Tensor(x.data.shape).normal_().cuda()
        x.data += self.fluctuation * rand
        x.data = x.data / maximum * half_span
        return x
