import torch
import torch.cuda
import torch.nn as nn
import torch.utils.data


class IOConstraint(nn.Module):
    def __init__(self, io_bits):
        super(IOConstraint, self).__init__()
        self.bits = io_bits
        return

    def forward(self, x):
        _max = torch.max(torch.abs(x))
        maximum = (2 ** self.bits - 1) / 2
        x.data = x.data / float(_max) * maximum
        x.data = torch.round(x.data)
        x.data = x.data / maximum * float(_max)
        return x
