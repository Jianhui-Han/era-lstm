import h5py
import time
import torch.nn as nn
import torch.utils.data

import timit_model

import argparse


# ==== Check carefully! ===================

log_interval = 11

add_noise = True
quantize_bit = 8
io_bits = 8
variation = 0
fluctuation = 0

rmax = 1

is_ft = True
base_model = 'model/timit_ewumlp_bsl_8355'
is_ft_ewu = False

parser = argparse.ArgumentParser(description='fine-tuning timit model')
parser.add_argument('--ew', type=str, default='')
args = parser.parse_args()

_, _, temp = args.ew.split('/', 3)
model_save = 'model/timit_new/' + temp + '_'
print('saving to model: %s' % model_save)

# ========================================

ninp = 39
nhid = 128
nout = 61

nepoch = 15
lr = 0.005
total_len = 4620
train_len = 3696
val_len = 400
test_len = 192
train_frame = 777
val_frame = 742
test_frame = 619
train_bsz = 24
val_bsz = 10
test_bsz = 8

torch.manual_seed(17)


def evaluate(data_loader, bsz, nframe, nlen):
    sum_all = 0
    with torch.no_grad():
        for i, (data_input, data_target) in enumerate(data_loader):
            sum_temp = 0
            inputs = data_input.view(bsz, nframe, ninp).cuda()
            targets = data_target.type(torch.LongTensor).cuda()
            outputs = model(inputs)
            _, results = torch.max(outputs.reshape(bsz*nframe, nout), 1)
            sum_temp += (targets.view(-1) == results).cpu().sum()
            sum_all += sum_temp.type(torch.FloatTensor)
        return 100. * sum_all / (nlen * nframe * 1.0)


def train():
    start_time = time.time()
    total_loss = 0.
    for i, (data_input, data_target) in enumerate(train_loader):
        inputs = data_input.view(train_bsz, train_frame, ninp).cuda()
        targets = data_target.type(torch.LongTensor).cuda()

        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs.reshape(train_bsz*train_frame, nout), targets.view(-1))
        total_loss += loss.item()

        loss.backward()
        optimizer.step()

        if i % log_interval == 0 and i > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:3d}/{:3d} batches | lr {:02.2f} | ms/batch {:5.2f} | '
                  'loss {:5.2f} '.format(epoch, i, train_len//train_bsz, lr, elapsed*1000/log_interval, cur_loss))
            total_loss = 0
            start_time = time.time()
    return total_loss / train_len


fin = h5py.File("timit_data/timit_mfcc_170904.h5", "r")
train_data = torch.utils.data.TensorDataset(
    torch.transpose(torch.from_numpy(fin["reduced"]["training"]["default"].value.astype('float32')), 0, 1),
    torch.transpose(torch.from_numpy(fin["reduced"]["training"]["targets"].value.astype('float32')), 0, 1))
train_loader = torch.utils.data.DataLoader(
    dataset=train_data, batch_size=train_bsz, shuffle=False, pin_memory=True)

validation_data = torch.utils.data.TensorDataset(
    torch.transpose(torch.from_numpy(fin["reduced"]["validation"]["default"].value.astype('float32')), 0, 1),
    torch.transpose(torch.from_numpy(fin["reduced"]["validation"]["targets"].value.astype('float32')), 0, 1))
validation_loader = torch.utils.data.DataLoader(
    dataset=validation_data, batch_size=val_bsz, shuffle=False, pin_memory=True)

test_data = torch.utils.data.TensorDataset(
    torch.transpose(torch.from_numpy(fin["reduced"]["test"]["default"].value.astype('float32')), 0, 1),
    torch.transpose(torch.from_numpy(fin["reduced"]["test"]["targets"].value.astype('float32')), 0, 1))
test_loader = torch.utils.data.DataLoader(
    dataset=test_data, batch_size=test_bsz, shuffle=False, pin_memory=True)
fin.close()

model = timit_model.TIMIT(ninp, nhid, nout, args.ew, rmax).cuda()

if is_ft:
    print('Fine-tuning: use model ' + base_model)
    with open(base_model, 'rb') as f:
        model_old = torch.load(f)
    model.linear_ih.weight = model_old.linear_ih.weight
    model.linear_ih.bias = model_old.linear_ih.bias
    model.linear_hh.weight = model_old.linear_hh.weight
    model.linear_hh.bias = model_old.linear_hh.bias
    model.linear_ih_re.weight = model_old.linear_ih_re.weight
    model.linear_ih_re.bias = model_old.linear_ih_re.bias
    model.linear_hh_re.weight = model_old.linear_hh_re.weight
    model.linear_hh_re.bias = model_old.linear_hh_re.bias
    model.linear_fc.weight = model_old.linear_fc.weight
    model.linear_fc.bias = model_old.linear_fc.bias
    model.is_ft_ewu = is_ft_ewu
model.add_noise = add_noise
model.io_bits = io_bits
model.quantize_bits = quantize_bit
# model.ewu.add_noise = add_noise
# model.ewu.quantize_bits = args.qb

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr)


prev_avg_loss = 0
best_val_acc = None
for epoch in range(1, nepoch + 1):
    epoch_start_time = time.time()
    avg_loss = train()
    val_acc = evaluate(validation_loader, val_bsz, val_frame, val_len)
    print('-' * 80)
    print('| end of epoch {:3d} | time: {:5.2f}s | valid acc {:5.2f} '
          .format(epoch, (time.time() - epoch_start_time), val_acc))
    print('-' * 80)

    if epoch > 1:
        if avg_loss >= prev_avg_loss:
            lr /= 2
            for param_group in optimizer.param_groups:
                param_group['lr'] = lr
        else:
            lr *= 1.01
            for param_group in optimizer.param_groups:
                param_group['lr'] = lr
        previous_average_loss = avg_loss
    else:
        previous_average_loss = avg_loss

    if not best_val_acc or val_acc > best_val_acc:
        with open(model_save, 'wb') as f:
            torch.save(model, f)
        best_val_acc = val_acc


with open(model_save, 'rb') as f:
    model = torch.load(f)
    print('loaded model: ', model_save)
    # model.add_noise = add_noise
    # model.io_bits = io_bits
    # model.quantize_bits = quantize_bits
    # model.variation = variation
    # model.fluctuation = fluctuation
    # model.ewu.add_noise = add_noise
    # model.ewu.quantize_bits = quantize_bits

# Run on test data.
test_acc = evaluate(test_loader, test_bsz, test_frame, test_len)
print('=' * 80)
print('| End of training | test acc {:5.2f} '.format(test_acc))
print('=' * 80)
print('saving to model: %s' % model_save)
