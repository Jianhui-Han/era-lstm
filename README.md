# Source code of ERA-LSTM

This is source code of ERA-LSTM. Refer to [ERA-LSTM](https://ieeexplore.ieee.org/document/8944023) for more details.

## Dependences

- PyTorch
- h5py (for TIMIT)

## Running

1. Before running TIMIT/PTB experiment, an EWU model should be trained first:

   ```shell
   python ewu_mlp.py
   ```

2. Then edit the head part of `timit.py`(L13-L33) and `ptb.py`(L10-L34) to configure the running.

3. To run the TIMIT experiment:

   ```shell
   python timit.py
   ```

   To run the PTB experiment:

   ```shell
   python ptb.py
   ```

   