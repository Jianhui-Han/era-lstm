import torch
import torch.cuda
import torch.nn as nn
import torch.utils.data
from torch.autograd import Variable
import time


def quantize_weight(_module, noise, quantize_bits):
    """
    quantize weights with noise
    :param _module:
    :param noise:
    :param quantize_bits:
    :return save_param:
    """
    if not isinstance(_module, nn.Module):
        print("_module should be nn.Module")
        return
    print("Quantizing weight with bits: ", quantize_bits)
    print("Add noise is: ", noise)
    start = time.clock()
    save_param = []
    for param in _module.parameters():
        a = Variable(param.data).data.cpu().numpy()
        if a.ndim == 2:
            _max = a.max()
            _min = a.min()
            # half_span = 0.0
            if abs(_max) > abs(_min):
                half_span = abs(_max)
            else:
                half_span = abs(_min)
            maximum = (2**quantize_bits-1)/2
            save_param.append(param.data)
            param.data = param.data / float(half_span) * maximum
            param.data = torch.round(param.data)
            param.data = param.data / maximum   # * float(half_span)
            param.data += torch.randn(a.shape).cuda()*(
                -0.0006034 * (100 * param.data + 4) ** 2 + 0.06184 * (100 * param.data + 4) + 0.7240) / 100
            param.data *= float(half_span)
    print("End of quantizing, time usage:", time.clock() - start)
    return save_param


def add_noise(module, params, variation=0.):
    if variation == 0.:
        for param in module.parameters():
            a = Variable(param.data).data.cpu().numpy()
            if a.ndim == 2:
                params.append(param.data)
                _max = a.max()
                _min = a.min()
                # half_span = 0.0
                if abs(_max) > abs(_min):
                    half_span = abs(_max)
                else:
                    half_span = abs(_min)
                param.data = param.data / float(half_span)
                param.data += torch.randn(a.shape).cuda() * (
                        -0.0006034 * (100 * param.data + 4) ** 2 + 0.06184 * (100 * param.data + 4) + 0.7240) / 100
                param.data *= float(half_span)
    else:
        pass


def store_param(module, params, counter):
    for param in module.parameters():
        a = Variable(param.data).data.cpu().numpy()
        if a.ndim == 2:
            param.data = params[counter]
            counter += 1
    return counter
